package fr.ul.iutmetz.info.wce.boutiqueandroid.dao;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;

import static org.junit.Assert.*;

/**
 * Created by Ricardo on 01/02/2018.
 */

public class DaoPromotionImplTest {

    @Test
    public void addPromotionTest() throws ParseException {
        String strDate = "2018-03-01";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateFinPromotion = dateFormat.parse(strDate);
        Promotion promo = new Promotion(19,new Date(),dateFinPromotion,20);
        new DaoPromotionImpl().add(promo,null);
    }
}
