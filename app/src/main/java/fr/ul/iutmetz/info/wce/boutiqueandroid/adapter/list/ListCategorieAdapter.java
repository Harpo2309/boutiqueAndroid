package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DeleteDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.categorie.CategorieUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;

/**
 * Created by Ricardo on 17/01/2018.
 */

public class ListCategorieAdapter extends ArrayAdapter<Categorie> implements View.OnClickListener, Serializable, DialogInterface.OnClickListener {

    private Context context;
    private FragmentManager fragmentManager;
    private Drawable substitut;
    private ArrayList<Categorie> categorieArrayList;
    private int position;

    public ListCategorieAdapter(Context context,FragmentManager fragmentManager, ArrayList<Categorie> categorieArrayList) {
        super(context,0,categorieArrayList);
        this.context = context;
        this.categorieArrayList = categorieArrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.list_item_categorie,viewGroup,false);
        }
        initUI(convertView, getItem(position),position);
        return convertView;
    }

    private  void initUI(View convertView, Categorie categorie, final int position) {

        ImageView icon = convertView.findViewById(R.id.cl_icon);
        ImageView edit = convertView.findViewById(R.id.cl_edit_category);
        ImageView delete = convertView.findViewById(R.id.cl_delete_category);
        TextView categorieNom = convertView.findViewById(R.id.cl_nom);

        delete.setTag(String.valueOf(position));
        edit.setTag(String.valueOf(position));
        categorieNom.setText(categorie.getNom());

        // Setting images
        Picasso.with(context)
                .load(API.BASE_ICONS_CATEGORIE + categorie.getVisuel() + API.EXTENSION_ICONS).into(icon);

        if (icon.getDrawable() == null) {
            try {
                InputStream inputStream = context.getAssets().open("not_found.png");
                this.substitut = Drawable.createFromStream(inputStream,null);
                icon.setImageDrawable(substitut);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        try {
            InputStream inputStream = context.getAssets().open("edit.png");
            this.substitut = Drawable.createFromStream(inputStream,null);
            edit.setImageDrawable(substitut);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            InputStream inputStream = context.getAssets().open("delete.png");
            this.substitut = Drawable.createFromStream(inputStream,null);
            delete.setImageDrawable(substitut);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        delete.setOnClickListener(this);
        edit.setOnClickListener(this);
    }

    /**
     * OnClickListener for Edit and Delete buttons
     * @param view Button
     */
    @Override
    public void onClick(View view) {
        Bundle args;
        position = Integer.parseInt(view.getTag().toString());
        switch (view.getId()) {
            // Updating
            case R.id.cl_edit_category:
                CategorieUpdateDialog dialogUpdate = new CategorieUpdateDialog();
                args = new Bundle();
                args.putSerializable("categorie",getItem(position));
                dialogUpdate.setArguments(args);
                dialogUpdate.show(fragmentManager,"update");
                break;
            // Deleting
            case R.id.cl_delete_category:
                DeleteDialog dialogDelete = new DeleteDialog();
                args = new Bundle();
                args.putString("typeItem","category");
                args.putSerializable("listenerFromAdapter",this);
                dialogDelete.setArguments(args);
                dialogDelete.show(fragmentManager,"delete");
                break;
        }
    }

    /**
     * OnClickListener for DialogFragment Delete
     * @param dialogInterface ?
     * @param i ?
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Categorie categorie = Categorie.fromIdCategorie(getItem(position).getId_categorie());
        DaoCategorieImpl.getInstance().delete(categorie,getContext());
        categorieArrayList.remove(getItem(position));
        notifyDataSetChanged();
    }
}
