package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list;

import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoClientImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.client.ClientUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DeleteDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;

/**
 * Created by Ricardo on 24/01/2018.
 */

public class ListClientAdapter extends ArrayAdapter<Client> implements View.OnClickListener, Serializable, DialogInterface.OnClickListener {

    private Context context;
    private FragmentManager fragmentManager;
    private ArrayList<Client> clientArrayList;
    private int position;

    public ListClientAdapter(Context context, FragmentManager fragmentManager, ArrayList<Client> clientArrayList) {
        super(context,0,clientArrayList);
        this.context = context;
        this.clientArrayList = clientArrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(getContext()).
                inflate(R.layout.list_item_client,viewGroup,false);
        initUI(convertView, getItem(position),position);
        return convertView;
    }

    private void initUI(View convertView, final Client client, final int position) {

        TextView nomClient = convertView.findViewById(R.id.cl_nom_client);
        TextView prenomClient = convertView.findViewById(R.id.cl_prenom_client);
        TextView villeClient = convertView.findViewById(R.id.cl_ville_client);
        ImageView editButton = convertView.findViewById(R.id.cl_edit_client);
        ImageView deleteButton = convertView.findViewById(R.id.cl_delete_client);

        editButton.setTag(String.valueOf(position));
        deleteButton.setTag(String.valueOf(position));
        nomClient.setText(client.getNom());
        prenomClient.setText(client.getPrenom());
        villeClient.setText(client.getVille());

        try {
            InputStream instmEdit = context.getAssets().open("edit.png");
            InputStream instmDelete = context.getAssets().open("delete.png");

            Drawable editIcon = Drawable.createFromStream(instmEdit,null);
            Drawable deleteIcon = Drawable.createFromStream(instmDelete,null);

            editButton.setImageDrawable(editIcon);
            deleteButton.setImageDrawable(deleteIcon);
            notifyDataSetChanged();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        editButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
    }

    /**
     * OnClickListener for DialogFragment Delete
     * @param dialogInterface
     * @param i
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Client client = Client.fromIdClient(getItem(position).getId_client());
        DaoClientImpl.getInstance().delete(client,getContext());
        clientArrayList.remove(getItem(position));
        notifyDataSetChanged();
    }

    /**
     * onClickListener for Edit and Delete buttons
     * @param view Button
     */
    @Override
    public void onClick(View view) {
        Bundle args;
        position = Integer.parseInt(view.getTag().toString());
        switch (view.getId()) {
            case R.id.cl_delete_client:
                DeleteDialog deleteDialog = new DeleteDialog();
                args = new Bundle();
                args.putInt("idItem",getItem(position).getId_client());
                args.putString("typeItem","client");
                args.putSerializable("listenerFromAdapter",this);
                deleteDialog.setArguments(args);
                deleteDialog.show(fragmentManager,"delete");
                notifyDataSetChanged();
                break;
            case R.id.cl_edit_client:
                ClientUpdateDialog updateDialog = new ClientUpdateDialog();
                args = new Bundle();
                args.putSerializable("client",getItem(position));
                updateDialog.setArguments(args);
                updateDialog.show(fragmentManager,"update");
                notifyDataSetChanged();
                break;
        }
    }
}
