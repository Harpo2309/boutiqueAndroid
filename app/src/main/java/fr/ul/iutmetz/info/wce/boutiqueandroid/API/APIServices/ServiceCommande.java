package fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Commande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import java.util.Date;

/**
 * Created by Celica on 23/01/2018.
 */

public interface ServiceCommande {

    @GET("findAll.php")
    Call<ArrayList<Commande>> listCommande();

    @GET("update.php")
    Call<OperationStatus> updateCommande(@Query("idCommande") int idCommande, @Query("date") String date,
                                        @Query("idClient") int idClient);

    @GET("delete.php")
    Call<OperationStatus> deleteCommande(@Query("idCommande") int idCommande);


    @GET("insert.php")
    Call<OperationStatus> addCommande(@Query("date") String date, @Query("idClient") int idClient);

}
