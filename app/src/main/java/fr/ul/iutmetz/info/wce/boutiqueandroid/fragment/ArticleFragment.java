package fr.ul.iutmetz.info.wce.boutiqueandroid.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceArticle;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListArticleAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ArticleFragment extends Fragment {
    private ListView listViewArticle;
    private ServiceArticle serviceArticle;
    private Call<ArrayList<Article>> listCall;
    private ListArticleAdapter adapter;

    public ArticleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        serviceArticle = API.getApi(API.ARTICLE).create(ServiceArticle.class);
        initUI(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAll();
    }

    private void initUI(View view) {
        listViewArticle = view.findViewById(R.id.listArticle);
    }

    public void getAll() {
        try {
            listCall = serviceArticle.listArticles();
            listCall.enqueue(new Callback<ArrayList<Article>>() {
                @Override
                public void onResponse(Call<ArrayList<Article>> call, Response<ArrayList<Article>> response) {
                    if (getContext() != null) {
                        adapter = new ListArticleAdapter(getContext(), getFragmentManager(), response.body());
                        listViewArticle.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<Article>> call, Throwable t) {
                    if (getContext() != null){
                        Toast.makeText(getContext(), R.string.error_internet , Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (NullPointerException npe) {
            Toast.makeText(getContext(), R.string.operation_error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public String toString() {
        return "ArticleFragment";
    }

}
