package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;

/**
 * Created by Celica on 09/01/2018.
 */

public class Categorie implements Serializable, Comparable<Categorie>{
    private int id_categorie;
    private String nom;
    private String visuel;

    public Categorie (){}

    private static Categorie instance;

    private Categorie(int id_categorie) {
        this.setId_categorie(id_categorie);
    }

    public static Categorie fromIdCategorie(int id_categorie) {
        if (instance == null)
            instance = new Categorie(id_categorie);
        else
            instance.setId_categorie(id_categorie);
        return instance;
    }

    public Categorie( String nom, String visuel){
        this.setNom(nom);
        this.setVisuel(visuel);
    }

    public Categorie(int id_categorie, String nom, String visuel){
        this.setId_categorie(id_categorie);
        this.setNom(nom);
        this.setVisuel(visuel);
    }

    public int getId_categorie() {
        return id_categorie;
    }

    public void setId_categorie(int id_categorie) {
        if (id_categorie <= 0) {
            throw new IllegalArgumentException("L'identifiant de la categorie n'est pas valide");
        }
        this.id_categorie = id_categorie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        if (nom==null || nom.trim().length()==0) {
            throw new IllegalArgumentException("Le nom est vide !");
        }
        this.nom = nom;
    }

    public String getVisuel() {
        return visuel;
    }

    public void setVisuel(String visuel) {
        this.visuel = visuel;
    }

    public static Categorie parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Categorie categorie = gson.fromJson(response,Categorie.class);
        return  categorie;
    }

    @Override
    public String toString() {
        return getVisuel();
    }

    @Override
    public int compareTo(@NonNull Categorie categorie) {
        return nom.compareTo(categorie.getNom());
    }

    @Override
    public boolean equals(Object object){
        if (object instanceof Categorie) {
            return ((Categorie) object).getId_categorie() == this.id_categorie;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getNom().hashCode();
    }
}
