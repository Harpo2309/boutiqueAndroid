package fr.ul.iutmetz.info.wce.boutiqueandroid.model;


import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ricardo on 23/01/2018.
 */

public class Visuel implements Serializable, Comparable<Visuel> {

    private String nomVisuel;

    private static Visuel instance;

    private Visuel(String nomVisuel) {
        this.nomVisuel = nomVisuel;
    }

    public static Visuel fromNomVisuel(String nomVisuel) {
        if (instance == null) {
            instance = new Visuel(nomVisuel);
        } else {
            instance.setNomVisuel(nomVisuel);
        }
        return instance;
    }


    public String getNomVisuel() {
        return nomVisuel;
    }

    public void setNomVisuel(String nomVisuel) {
        this.nomVisuel = nomVisuel;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Visuel) {
            return ((Visuel) object).getNomVisuel().equals(this.nomVisuel);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getNomVisuel().hashCode();
    }

    @Override
    public String toString() {
        return this.nomVisuel;
    }

    @Override
    public int compareTo(@NonNull Visuel visuel) {
        return nomVisuel.compareTo(visuel.getNomVisuel());
    }
}
