package fr.ul.iutmetz.info.wce.boutiqueandroid.util;

import android.content.Context;
import android.view.View;

/**
 * Created by Ricardo on 09/01/2018.
 */

public interface DAO <T>{
    void add(T object, Context context);
    void update(T object, Context context);
    void delete(T object, Context context);
}
