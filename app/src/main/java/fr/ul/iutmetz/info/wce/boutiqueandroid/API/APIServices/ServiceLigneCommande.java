package fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Ligne_Commande;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Ricardo on 02/03/2018.
 */

public interface ServiceLigneCommande {

    @GET("findAll.php")
    Call<ArrayList<Ligne_Commande>> listLigneCommande();

}
