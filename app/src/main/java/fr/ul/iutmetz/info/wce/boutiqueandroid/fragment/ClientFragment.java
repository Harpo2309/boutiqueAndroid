package fr.ul.iutmetz.info.wce.boutiqueandroid.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceClient;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListClientAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ClientFragment extends Fragment {

    private ListView listViewClient;
    private ServiceClient serviceClient;
    private Call<ArrayList<Client>> listCall;
    private ListClientAdapter adapter;

    public ClientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client, container, false);
        serviceClient = API.getApi(API.CLIENT).create(ServiceClient.class);
        initUI(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAll();
    }

    private void initUI(View view) {
        listViewClient = view.findViewById(R.id.listClient);
    }

    public void getAll() {
        try {
            listCall = serviceClient.listClients();
            listCall.enqueue(new Callback<ArrayList<Client>>() {
                @Override
                public void onResponse(Call<ArrayList<Client>> call, Response<ArrayList<Client>> response) {
                    if (getContext() != null) {
                        adapter = new ListClientAdapter(getContext(), getFragmentManager(), response.body());
                        listViewClient.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<Client>> call, Throwable t) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), R.string.error_internet , Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (NullPointerException npe) {
            Toast.makeText(getContext(), R.string.operation_error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public String toString() {
        return "ClientFragment";
    }

}
