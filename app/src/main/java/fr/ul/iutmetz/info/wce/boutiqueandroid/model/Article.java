package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;

/**
 * Created by Celica on 09/01/2018.
 */

public class Article implements Serializable{
    private int id_article;
    private String reference;
    private String nom;
    private double tarif;
    private String visuel;
    private int idCategorie;

    private static Article instance;

    public Article() {
    }

    protected Article(int id_article) {
        this.setId_article(id_article);
    }

    public static Article fromIdArticle(int id_article){
        if (instance == null)
            instance = new Article(id_article);
        else
            instance.setId_article(id_article);
        return instance;
    }

    public Article(String reference, String nom, double tarif, String visuel, int idCategorie){
        this.setReference(reference);
        this.setNom(nom);
        this.setTarif(tarif);
        this.setVisuel(visuel);
        this.setIdCategorie(idCategorie);
    }

    public Article(int id_article, String reference, String nom, double tarif, String visuel, int idCategorie){
        this.setId_article(id_article);
        this.setReference(reference);
        this.setNom(nom);
        this.setTarif(tarif);
        this.setVisuel(visuel);
        this.setIdCategorie(idCategorie);
    }

    public int getId_article() {
        return id_article;
    }

    public void setId_article(int id_article) {
        this.id_article = id_article;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        if (reference==null || reference.trim().length()==0) {
            throw new IllegalArgumentException("La reference est vide !");
        }
        this.reference = reference;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        if (nom==null || nom.trim().length()==0) {
        throw new IllegalArgumentException("Le nom est vide !");
        }
        this.nom = nom;
    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        if (tarif > 0) {
            this.tarif = tarif;
        } else {
            throw new IllegalArgumentException("setMontant : somme négative : "
                    + tarif);
        }

    }

    public String getVisuel() {
        return visuel;
    }

    public void setVisuel(String visuel) {
        if (visuel==null || visuel.trim().length()==0) {
            throw new IllegalArgumentException("Le lien est vide !");
        }
        this.visuel = visuel;
    }

    public int getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    public static Article parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Article article = gson.fromJson(response,Article.class);
        return  article;
    }

    @Override
    public String toString() {
        return getVisuel();
    }
}
