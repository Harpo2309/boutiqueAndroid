package fr.ul.iutmetz.info.wce.boutiqueandroid.dao;

import android.content.Context;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServicePromotion;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.DAO;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ricardo on 01/02/2018.
 */

public class DaoPromotionImpl implements DAO<Promotion> {

    private Call<OperationStatus> operationStatusCall;
    private ServicePromotion servicePromotion;

    private int idArticle;
    private String dateDebutPromotion;
    private String dateFinPromotion;
    private int pourcentageReduction;

    private static DaoPromotionImpl instance;

    private DaoPromotionImpl() {
        servicePromotion = API.getApi(API.PROMOTION).create(ServicePromotion.class);
    }

    public static DaoPromotionImpl getInstance() {
        if (instance == null) {
            instance = new DaoPromotionImpl();
        }
        return instance;
    }

    @Override
    public void add(Promotion object, final Context context) {
        try{
            initValuesFrom(object);
            operationStatusCall = servicePromotion.addPromotion(
                    this.idArticle,
                    this.dateDebutPromotion,
                    this.dateFinPromotion,
                    this.pourcentageReduction
            );
            operationStatusCall.enqueue(new Callback<OperationStatus>() {
                @Override
                public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, R.string.operation_add_success,Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OperationStatus> call, Throwable t) {
                    Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void update(Promotion object, final Context context) {

       try{
           initValuesFrom(object);
           operationStatusCall = servicePromotion.uodatePromotion(
                   this.dateDebutPromotion,
                   this.dateFinPromotion,
                   this.pourcentageReduction,
                   this.idArticle
           );
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_update_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException | IllegalArgumentException ex) {
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public void delete(Promotion object, final Context context) {
        try{
            operationStatusCall = servicePromotion.deletePromotion(object.getId_article());
            operationStatusCall.enqueue(new Callback<OperationStatus>() {
                @Override
                public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, R.string.operation_delete_success,Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OperationStatus> call, Throwable t) {
                    Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }

    private void initValuesFrom(Promotion object) {
        this.idArticle = object.getId_article();
        this.dateDebutPromotion = object.getDateDebutPromotion();
        this.dateFinPromotion = object.getDateFinPromotion();
        this.pourcentageReduction = object.getPourcentageReduction();
    }

}
