package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.article;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceArticle;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerCategorieAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerVisuelArticleAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoArticleImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel_Article;
import retrofit2.Call;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Celica on 24/01/2018.
 */

public class ArticleUpdateDialog extends DialogFragment {

    private Article article;
    private Categorie categorie;
    private Spinner spinnerArticle;
    private Spinner spinnerCategorie;
    private ServiceArticle serviceArticle;
    private ServiceCategorie serviceCategorie;
    private Call<ArrayList<Visuel_Article>> listCall;
    private Call<ArrayList<Categorie>> listCallCategorie;
    private SpinnerVisuelArticleAdapter adapter;
    private SpinnerCategorieAdapter adapterAllCategorie;
    private EditText editTextReference;
    private EditText editTextNom;
    private EditText editTextTarif;
    private TextView title;
    private ArrayList<Visuel_Article> visuelResponseArticle;
    private ArrayList<Categorie> visuelResponseCategorie;
    public ArticleUpdateDialog() {
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_article_update,null);

        setArticle((Article) getArguments().getSerializable("article"));

        editTextReference = viewUpdateDialog.findViewById(R.id.et_reference);
        editTextNom = viewUpdateDialog.findViewById(R.id.et_nom);
        editTextTarif = viewUpdateDialog.findViewById(R.id.et_tarif);
        title = viewUpdateDialog.findViewById(R.id.title_dialog_article);

        title.setText(R.string.df_article_update);
        editTextReference.setText(article.getReference());
        editTextNom.setText(article.getNom());
        editTextTarif.setText(Double.toString(article.getTarif()));


        serviceArticle  = API.getApi(API.VISUEL_ARTICLE).create(ServiceArticle.class);
        // Spiner pour l'image du article
       try{
           listCall = serviceArticle.listVisuelArticle();
           listCall.enqueue(new Callback<ArrayList<Visuel_Article>>() {
               @Override
               public void onResponse(Call<ArrayList<Visuel_Article>> call, Response<ArrayList<Visuel_Article>> response) {
                  if (getContext() != null){
                      adapter = new SpinnerVisuelArticleAdapter(getContext(),getFragmentManager(),response.body());
                      visuelResponseArticle = response.body();
                      adapter.setDropDownViewResource(R.layout.spinner_item_article);
                      spinnerArticle = getDialog().findViewById(R.id.sp_article);
                      spinnerArticle.setAdapter(adapter);
                      Visuel_Article currentVisuel = Visuel_Article.fromVisuel(article.getVisuel());
                      int currentValue = visuelResponseArticle.indexOf(currentVisuel);
                      spinnerArticle.setSelection(currentValue);
                  }
               }
               @Override
               public void onFailure(Call<ArrayList<Visuel_Article>> call, Throwable t) {
                  if(getContext() != null){
                      Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                  }
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
       }

        // Spiner pour choisir la categorie
        setCategorie((Categorie) getArguments().getSerializable("categorie"));
        serviceCategorie  = API.getApi(API.CATEGORIE).create(ServiceCategorie.class);

        try{
            listCallCategorie = serviceCategorie.listCategorie();
            listCallCategorie.enqueue(new Callback<ArrayList<Categorie>>() {
                @Override
                public void onResponse(Call<ArrayList<Categorie>> call, Response<ArrayList<Categorie>> response) {
                   if (getContext() != null){
                       adapterAllCategorie = new SpinnerCategorieAdapter(getContext(),getFragmentManager(),response.body());
                       visuelResponseCategorie = response.body();
                       adapterAllCategorie.setDropDownViewResource(R.layout.spinner_nom_categorie);
                       spinnerCategorie = getDialog().findViewById(R.id.sp_all_categorie);
                       spinnerCategorie.setAdapter(adapterAllCategorie);
                       Categorie currentCategorie = Categorie.fromIdCategorie(article.getIdCategorie());
                       int currentValue = visuelResponseCategorie.indexOf(currentCategorie);
                       spinnerCategorie.setSelection(currentValue);
                   }
                }
                @Override
                public void onFailure(Call<ArrayList<Categorie>> call, Throwable t) {
                    if (getContext() != null){
                        Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
        }

        builder.setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                editTextReference = getDialog().findViewById(R.id.et_reference);
                editTextNom = getDialog().findViewById(R.id.et_nom);
                editTextTarif = getDialog().findViewById(R.id.et_tarif);
                try {
                    getArticle().setReference(editTextReference.getText().toString());
                    getArticle().setNom(editTextNom.getText().toString()
                            .replaceFirst(String.valueOf(editTextNom.getText()
                                    .charAt(0)),String.valueOf(editTextNom.getText()
                                    .charAt(0)).toUpperCase()));
                    getArticle().setTarif(Double.parseDouble(editTextTarif.getText().toString()));
                    article.setVisuel(spinnerArticle.getSelectedItem().toString());
                    categorie =  (Categorie)spinnerCategorie.getSelectedItem();
                    article.setIdCategorie(categorie.getId_categorie());
                    DaoArticleImpl.getInstance().update(article,getContext());
                } catch (NullPointerException | IllegalArgumentException | IndexOutOfBoundsException iae) {
                    Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
                }
            }
        })
                .setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                });

        builder.setView(viewUpdateDialog);
        return builder.create();
    }


    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

}
