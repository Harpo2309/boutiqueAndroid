package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.fragment.ArticleFragment;
import fr.ul.iutmetz.info.wce.boutiqueandroid.fragment.CategorieFragment;
import fr.ul.iutmetz.info.wce.boutiqueandroid.fragment.ClientFragment;
import fr.ul.iutmetz.info.wce.boutiqueandroid.fragment.CommandeFragment;
import fr.ul.iutmetz.info.wce.boutiqueandroid.fragment.PromotionFragment;

/**
 * Created by Ricardo on 17/01/2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int tabNumber;
    private int menuType;

    // CONSTANTS
    private static final int MENU_BOUTIQUE = 0;
    private static final int MENU_SALES = 1;

    public PagerAdapter(FragmentManager fm, int tabNumber, int menuType) {
        super(fm);
        this.tabNumber = tabNumber;
        this.menuType = menuType;
    }

    /**
     * Return an instance of a fragment
     * @param position fragment's location
     * @return An instance of a selected fragment or null if there isn't a fragment mapped
     */
    @Override
    public Fragment getItem(int position) {
        switch(menuType) {
            case MENU_BOUTIQUE: // Menu Boutique
                switch(position) {
                    case 0:
                        return new CategorieFragment();
                    case 1:
                        return new ArticleFragment();
                    case 2:
                        return new PromotionFragment();
                    default:
                        return null;
                }
            case MENU_SALES: // Menu Sales
                switch (position) {
                    case 0:
                        return new ClientFragment();
                    case 1:
                        return new CommandeFragment();
                        default:
                            return null;
                }
            default:
                return null;
        }
    }

    public String getItemClassName(int position) {
        switch(menuType) {
            case MENU_BOUTIQUE: // Menu Boutique
                switch(position) {
                    case 0:
                        return new CategorieFragment().toString();
                    case 1:
                        return new ArticleFragment().toString();
                    case 2:
                        return new PromotionFragment().toString();
                    default:
                        return null;
                }
            case MENU_SALES: // Menu Sales
                switch (position) {
                    case 0:
                        return new ClientFragment().toString();
                    case 1:
                        return new CommandeFragment().toString();
                    default:
                        return null;
                }
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabNumber;
    }
}
