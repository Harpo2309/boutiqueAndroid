package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.commande;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCommande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerCategorieAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Commande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Célica on 01/03/2018.
 */

public class CommandeUpdateDialog extends DialogFragment {

    private Commande commande;
    private Spinner spinnerCommande;
    private ServiceCommande serviceCommande;
    private Call<ArrayList<Visuel>> listCall;
    private ArrayList<Visuel> visuelResponse;
    private SpinnerCategorieAdapter adapter;
    private EditText date;
    private EditText etNomClient;
    private EditText etPrenomClient;

    public CommandeUpdateDialog(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_commande_update,null);

        commande = (Commande) getArguments().getSerializable("commande");
        //date = viewUpdateDialog.findViewById(R.id.et_date);



        builder.setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

              /*  try {
                    commande.setClient(etNomClient);
                    categorie.setVisuel(spinnerCategorie.getSelectedItem().toString());
                    DaoCategorieImpl.getInstance().update(categorie,getContext());
                } catch (NullPointerException | IllegalArgumentException npe) {
                    Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
                }*/
            }
        })
        .setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        builder.setView(viewUpdateDialog);
        return builder.create();
    }
}
