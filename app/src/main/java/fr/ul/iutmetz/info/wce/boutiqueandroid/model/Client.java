package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;

/**
 * Created by Celica on 09/01/2018.
 */

public class Client implements Serializable {
    private int id_client;
    private String nom;
    private String prenom;
    private String ville;

    private static Client instance;

    public Client(){}

    private Client(int id_client) {
        this.setId_client(id_client);
    }

    public static Client fromIdClient(int id_client) {
        if (instance == null)
            instance = new Client(id_client);
        else
            instance.setId_client(id_client);
        return instance;
    }

    public Client(String nom, String prenom, String ville){
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setVille(ville);
    }

    public Client(int id_client, String nom, String prenom, String ville){
        this.setId_client(id_client);
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setVille(ville);
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        if (nom==null || nom.trim().length()==0) {
            throw new IllegalArgumentException("Le nom est vide !");
        }
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        if (prenom==null || prenom.trim().length()==0) {
            throw new IllegalArgumentException("Le prenom est vide !");
        }
        this.prenom = prenom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        if (ville==null || ville.trim().length()==0) {
            throw new IllegalArgumentException("La ville est vide !");
        }
        this.ville = ville;
    }

    public static Client parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Client client = gson.fromJson(response,Client.class);
        return  client;
    }
}
