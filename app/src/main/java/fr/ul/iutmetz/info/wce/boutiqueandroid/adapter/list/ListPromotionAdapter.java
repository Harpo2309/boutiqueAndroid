package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoPromotionImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DeleteDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.promotion.PromotionUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;

/**
 * Created by Ricardo on 02/02/2018.
 */

public class ListPromotionAdapter extends ArrayAdapter<Promotion> implements View.OnClickListener, Serializable, DialogInterface.OnClickListener {

    private Context context;
    private FragmentManager fragmentManager;
    private Drawable substitut;
    private ArrayList<Promotion> promotionArrayList;
    private int position;

    public ListPromotionAdapter(Context context, FragmentManager fragmentManager, ArrayList<Promotion> promotionArrayList) {
        super(context,0,promotionArrayList);
        this.context = context;
        this.promotionArrayList = promotionArrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.list_item_promotion,viewGroup,false);
        }
        initUI(convertView, getItem(position),position);
        return convertView;
    }

    private void initUI(View convertView, final Promotion promotion, final int position) {

        ImageView editButton = convertView.findViewById(R.id.li_promotion_edit);
        ImageView deleteButton = convertView.findViewById(R.id.li_promotion_delete);
        ImageView imageArticle = convertView.findViewById(R.id.promo_article_image);

        deleteButton.setTag(String.valueOf(position));
        editButton.setTag(String.valueOf(position));

        TextView nameArticleInPromo = convertView.findViewById(R.id.promo_article_name);
        TextView discountArticleInPromo = convertView.findViewById(R.id.promo_article_percentage_discount);
        TextView startPriceArticleInPromo = convertView.findViewById(R.id.promo_article_start_price);
        TextView discountPriceArticleInPromo = convertView.findViewById(R.id.promo_article_discount_price);
        TextView beginDateArticleInPromo = convertView.findViewById(R.id.promo_article_begin_date);
        TextView endDateArticleInPromo = convertView.findViewById(R.id.promo_article_end_date);

        // Setting images
        Picasso.with(context)
                .load(API.BASE_ICONS_ARTICLE + promotion.getVisuel() + API.EXTENSION_ICONS).into(imageArticle);

        if (imageArticle.getDrawable() == null) {
            try {
                InputStream inputStream = context.getAssets().open("not_found.png");
                this.substitut = Drawable.createFromStream(inputStream,null);
                imageArticle.setImageDrawable(substitut);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        try {
            InputStream instmEdit = context.getAssets().open("edit.png");
            InputStream instmDelete = context.getAssets().open("delete.png");

            Drawable editIcon = Drawable.createFromStream(instmEdit,null);
            Drawable deleteIcon = Drawable.createFromStream(instmDelete,null);

            editButton.setImageDrawable(editIcon);
            deleteButton.setImageDrawable(deleteIcon);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        NumberFormat numberFormatter = new DecimalFormat("#0.00");

        nameArticleInPromo.setText(promotion.getNom());
        discountArticleInPromo.setText(promotion.getPourcentageReduction() +"%");
        startPriceArticleInPromo.setText(promotion.getTarif() + "€");
        startPriceArticleInPromo.setPaintFlags(
                startPriceArticleInPromo.getPaintFlags()
                        | Paint.STRIKE_THRU_TEXT_FLAG);

        double discount = promotion.getTarif() * (promotion.getPourcentageReduction() * 0.01);
        discountPriceArticleInPromo.setText(
                numberFormatter.format(promotion.getTarif() - discount) + "€"
        );
        beginDateArticleInPromo.setText(
                promotion.getDateDebutPromotion()
        );
        endDateArticleInPromo.setText(
                promotion.getDateFinPromotion()
        );

        editButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
    }

    /**
     * OnClickListener for DialogFragment Delete
     * @param dialogInterface
     * @param i
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Promotion promotion = Promotion.fromIdArticle(getItem(position).getId_article());
        DaoPromotionImpl.getInstance().delete(promotion,getContext());
        promotionArrayList.remove(getItem(position));
        notifyDataSetChanged();
    }

    /**
     * OnClickListener for Edit and Delete buttons
     * @param view
     */
    @Override
    public void onClick(View view) {
        Bundle args;
        position = Integer.parseInt(view.getTag().toString());
        switch (view.getId()) {
            case R.id.li_promotion_delete:
                DeleteDialog deleteDialog = new DeleteDialog();
                args = new Bundle();
                args.putInt("idItem",getItem(position).getId_article());
                args.putString("typeItem","promotion");
                args.putSerializable("listenerFromAdapter",this);
                deleteDialog.setArguments(args);
                deleteDialog.show(fragmentManager,"delete");
                break;
            case R.id.li_promotion_edit:
                PromotionUpdateDialog updateDialog = new PromotionUpdateDialog();
                args = new Bundle();
                args.putSerializable("promotion",getItem(position));
                updateDialog.setArguments(args);
                updateDialog.show(fragmentManager,"update");
                notifyDataSetChanged();
                break;
        }
    }
}
