package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoArticleImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoClientImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoPromotionImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Commande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;

/**
 * Created by Ricardo on 21/01/2018.
 */

public class DeleteDialog extends DialogFragment {

    private String typeItem;
    private AlertDialog.Builder builder;
    private TextView dialogTitle;
    private TextView dialogAdvise1;
    private TextView dialogAdvise2;
    private DialogInterface.OnClickListener listenerFromAdapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        View viewDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_delete,null);

        typeItem = getArguments().getString("typeItem");
        listenerFromAdapter = (DialogInterface.OnClickListener)
                getArguments().getSerializable("listenerFromAdapter");

        initUIFor(typeItem,viewDialog);

        builder.setView(viewDialog);
        return builder.create();
    }

    private void initUIFor(final String typeItem, View viewDialog) {
        dialogTitle = viewDialog.findViewById(R.id.dialog_title);
        dialogAdvise1 = viewDialog.findViewById(R.id.text2);
        dialogAdvise2 = viewDialog.findViewById(R.id.text3);

        switch (typeItem) {
            case "category":
                dialogTitle.setText(R.string.df_category_delete);
                dialogAdvise1.setText(R.string.message_text2_category);
                dialogAdvise2.setText(R.string.message_text3_category);
                break;
            case "article":
                dialogTitle.setText(R.string.df_article_delete);
                dialogAdvise1.setText(R.string.message_text2);
                dialogAdvise2.setText(R.string.message_text3);
                break;
            case "promotion":
                dialogTitle.setText(R.string.df_promo_delete);
                break;
            case "client":
                dialogTitle.setText(R.string.df_client_delete);
                break;
            case "commande":
                dialogTitle.setText(R.string.df_commande_delete);
                break;
        }

        builder.setPositiveButton(R.string.action_yes,listenerFromAdapter);
        builder.setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
    }
}
