package fr.ul.iutmetz.info.wce.boutiqueandroid.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceClient;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServicePromotion;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListClientAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListPromotionAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionFragment extends Fragment {

    private ListView listViewPromotion;
    private ServicePromotion servicePromotion;
    private Call<ArrayList<Promotion>> listCall;
    private ListPromotionAdapter adapter;

    public PromotionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promotion, container, false);
        servicePromotion = API.getApi(API.PROMOTION).create(ServicePromotion.class);
        initUI(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAll();
    }

    private void initUI(View view) {
        listViewPromotion = view.findViewById(R.id.listPromotion);
    }

    public void getAll() {
        listCall = servicePromotion.listPromotion();
        try {
            listCall.enqueue(new Callback<ArrayList<Promotion>>() {

                @Override
                public void onResponse(Call<ArrayList<Promotion>> call, Response<ArrayList<Promotion>> response) {
                    if (getContext() != null) {
                        adapter = new ListPromotionAdapter(getContext(), getFragmentManager(), response.body());
                        listViewPromotion.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<Promotion>> call, Throwable t) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), R.string.error_internet , Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (NullPointerException npe) {
            Toast.makeText(getContext(), R.string.error_internet , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public String toString() {
        return "PromotionFragment";
    }

}
