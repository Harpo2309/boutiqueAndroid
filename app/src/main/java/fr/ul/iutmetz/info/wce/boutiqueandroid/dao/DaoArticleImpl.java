package fr.ul.iutmetz.info.wce.boutiqueandroid.dao;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceArticle;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.DAO;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by Celica on 23/01/2018.
 */

public class DaoArticleImpl implements DAO<Article>{

    private Call<ArrayList<Article>> listCall;
    private Call<OperationStatus> operationStatusCall;
    private ServiceArticle serviceArticle;
    private String reference;
    private String nom;
    private double tarif;
    private String visuel;
    private int idCategorie;
    private int idArt;

    public static DaoArticleImpl instance;

    private DaoArticleImpl() {
        serviceArticle  = API.getApi(API.ARTICLE).create(ServiceArticle.class);
    }

    public static DaoArticleImpl getInstance() {
        if (instance == null) {
            instance = new DaoArticleImpl();
        }
        return instance;
    }

    @Override
    public void add(Article object, final Context context) {
        reference = object.getReference();
        nom = object.getNom();
        tarif = object.getTarif();
        visuel = object.getVisuel();
        idCategorie = object.getIdCategorie();

       try{
           operationStatusCall = serviceArticle.addArticle(reference,nom,tarif,visuel,idCategorie);
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_add_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public void update(Article object, final Context context) {
        reference  = object.getReference();
        nom = object.getNom();
        tarif = object.getTarif();
        visuel = object.getVisuel();
        idCategorie = object.getIdCategorie();
        idArt = object.getId_article();
     try{
         operationStatusCall = serviceArticle.updateArticle(idArt,reference,nom,tarif,visuel,idCategorie);
         operationStatusCall.enqueue(new Callback<OperationStatus>() {
             @Override
             public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                 if (response.isSuccessful()) {
                     Toast.makeText(context, R.string.operation_update_success,Toast.LENGTH_SHORT).show();
                 }
             }

             @Override
             public void onFailure(Call<OperationStatus> call, Throwable t) {
                 Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
             }
         });
     }catch (NullPointerException npe){
         Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
     }
    }

    @Override
    public void delete(Article object, final Context context) {
        idArt = object.getId_article();
       try{
           operationStatusCall = serviceArticle.deleteArticle(idArt);
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   Toast.makeText(context, R.string.operation_delete_success,Toast.LENGTH_SHORT).show();
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }
}
