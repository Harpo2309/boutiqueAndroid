package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoArticleImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.article.ArticleUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DeleteDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Celica on 24/01/2018.
 */

public class ListArticleAdapter extends ArrayAdapter<Article> implements View.OnClickListener, Serializable, DialogInterface.OnClickListener {

    private Context context;
    private FragmentManager fragmentManager;
    private Drawable substitut;
    private ArrayList<Article> articleArrayList;
    private int position;

    public ListArticleAdapter(Context context,FragmentManager fragmentManager, ArrayList<Article> articleArrayList) {
        super(context,0,articleArrayList);
        this.context = context;
        this.articleArrayList = articleArrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.list_item_article,viewGroup,false);
        }
        initUI(convertView, getItem(position),position);
        return convertView;
    }

    private  void initUI(View convertView, Article article, final int position) {

        ImageView icon = convertView.findViewById(R.id.cl_icon_article);//Image d'article
        ImageView edit = convertView.findViewById(R.id.cl_edit_article);//Image d'icon pour edit
        ImageView delete = convertView.findViewById(R.id.cl_delete_article);//Image d'icon delete
        TextView articleReference = convertView.findViewById(R.id.cl_reference);//Reference d'article
        TextView articleNom = convertView.findViewById(R.id.cl_nom);//Nom d'article
        TextView articleTarif = convertView.findViewById(R.id.cl_tarif);//Tarif d'article

        delete.setTag(String.valueOf(position));
        edit.setTag(String.valueOf(position));
        articleReference.setText(article.getReference());
        articleNom.setText(article.getNom());
        articleTarif.setText(Double.toString(article.getTarif())+" €");

        // Setting images
        Picasso.with(context)
                .load(API.BASE_ICONS_ARTICLE + article.getVisuel() + API.EXTENSION_ICONS).into(icon);

        if (icon.getDrawable() == null) {
            try {
                InputStream inputStream = context.getAssets().open("not_found.png");
                this.substitut = Drawable.createFromStream(inputStream,null);
                icon.setImageDrawable(substitut);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        try {
            InputStream inputStream = context.getAssets().open("edit.png");
            this.substitut = Drawable.createFromStream(inputStream,null);
            edit.setImageDrawable(substitut);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            InputStream inputStream = context.getAssets().open("delete.png");
            this.substitut = Drawable.createFromStream(inputStream,null);
            delete.setImageDrawable(substitut);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        edit.setOnClickListener(this);
        delete.setOnClickListener(this);
    }

    /**
     * OnClickListener for Edit and Delete buttons
     * @param view Button
     */
    @Override
    public void onClick(View view) {
        Bundle args;
        position = Integer.parseInt(view.getTag().toString());
        switch (view.getId()) {
            // updating
            case R.id.cl_edit_article:
                ArticleUpdateDialog dialogUpdate = new ArticleUpdateDialog();
                args = new Bundle();
                args.putSerializable("article",getItem(position));
                dialogUpdate.setArguments(args);
                dialogUpdate.show(fragmentManager,"update");
                break;
            // deleting
            case R.id.cl_delete_article:
                DeleteDialog dialogDelete = new DeleteDialog();
                args = new Bundle();
                args.putString("typeItem","article");
                args.putSerializable("listenerFromAdapter",this);
                dialogDelete.setArguments(args);
                dialogDelete.show(fragmentManager,"delete");
                break;
        }
    }

    /**
     * OnClickListener for DialogFragment Delete
     * @param dialogInterface ?
     * @param i ?
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Article article = Article.fromIdArticle(getItem(position).getId_article());
        DaoArticleImpl.getInstance().delete(article,getContext());
        articleArrayList.remove(getItem(position));
        notifyDataSetChanged();
    }
}
