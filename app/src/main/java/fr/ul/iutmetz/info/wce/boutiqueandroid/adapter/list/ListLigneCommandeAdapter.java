package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.categorie.CategorieUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DeleteDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Ligne_Commande;

/**
 * Created by Ricardo on 17/01/2018.
 */

public class ListLigneCommandeAdapter extends ArrayAdapter<Ligne_Commande> {

    private Context context;
    private FragmentManager fragmentManager;
    private ArrayList<Ligne_Commande> ligneCommandeArrayList;

    public ListLigneCommandeAdapter(Context context, FragmentManager fragmentManager, ArrayList<Ligne_Commande> ligneCommandeArrayList) {
        super(context,0,ligneCommandeArrayList);
        this.context = context;
        this.ligneCommandeArrayList = ligneCommandeArrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.list_item_ligne_commande,viewGroup,false);
        }
        initUI(convertView, getItem(position));
        return convertView;
    }

    private  void initUI(View convertView, Ligne_Commande ligneCommande) {


        TextView reference = convertView.findViewById(R.id.li_reference);
        TextView quantite = convertView.findViewById(R.id.li_quantite_article);
        TextView nomArticle = convertView.findViewById(R.id.li_nomArticle);

        reference.setText(ligneCommande.getReference());
        nomArticle.setText(ligneCommande.getNomArticle());
        quantite.setText(ligneCommande.getQuantite() + "");
    }
}
