package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by Celica on 24/01/2018.
 */

public class Visuel_Article implements Serializable, Comparable<Visuel_Article>{
    private String visuel;

    public static Visuel_Article instance;

    public Visuel_Article(String visuel) {
        this.visuel = visuel;
    }

    public static Visuel_Article fromVisuel(String visuel) {
        if (instance == null) {
            instance = new Visuel_Article(visuel);
        } else {
            instance.setVisuel(visuel);
        }
        return instance;
    }

    @Override
    public boolean equals(Object object){
        if (object instanceof Visuel_Article) {
            return ((Visuel_Article) object).getVisuel().equals(this.visuel);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getVisuel().hashCode();
    }

    @Override
    public int compareTo(@NonNull Visuel_Article visuelArt) {
        return visuel.compareTo(visuelArt.getVisuel());
    }

    public String getVisuel() {
        return visuel;
    }

    public void setVisuel(String visuel) {
        this.visuel = visuel;
    }

    @Override
    public String toString() {
        return getVisuel();
    }
}
