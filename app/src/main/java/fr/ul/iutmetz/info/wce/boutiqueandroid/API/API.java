package fr.ul.iutmetz.info.wce.boutiqueandroid.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ricardo on 20/01/2018.
 */

public class API {

    // entities
    public static final String CATEGORIE = "Categorie/";
    public static final String ARTICLE = "Article/";
    public static final String PROMOTION = "Promotion/";
    public static final String CLIENT = "Client/";
    public static final String COMMANDE = "Commande/";
    public static final String VISUEL = "Visuel/";
    public static final String VISUEL_ARTICLE = "Visuel_Article/";
    public static final String LIGNE_COMMANDE = "Ligne_Commande/";

    public static final String BASE_URL = "https://infodb.iutmetz.univ-lorraine.fr/~cabreras1u/boutique/php/";
    public static final String BASE_ICONS_CATEGORIE = "https://infodb.iutmetz.univ-lorraine.fr/~cabreras1u/boutique/img/categorie/";
    public static final String BASE_ICONS_ARTICLE = "https://infodb.iutmetz.univ-lorraine.fr/~cabreras1u/boutique/img/article/";
    public static final String EXTENSION_ICONS = ".png";

    public static Retrofit getApi(String entity) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL + entity)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
