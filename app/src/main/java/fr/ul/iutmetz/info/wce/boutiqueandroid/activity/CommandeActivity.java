package fr.ul.iutmetz.info.wce.boutiqueandroid.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceLigneCommande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListLigneCommandeAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Ligne_Commande;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommandeActivity extends AppCompatActivity {

    private TextView clientData;
    private ListView ligneCommande;
    private ServiceLigneCommande serviceLigneCommande;
    private Call<ArrayList<Ligne_Commande>>  listCall;
    private ListLigneCommandeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commande);
        clientData = findViewById(R.id.client_data);
        ligneCommande = findViewById(R.id.list_commande);

        String clientName = getIntent().getStringExtra("clientName");
        String clientLastName = getIntent().getStringExtra("clientLastName");
        clientData.setText(clientLastName + " " + clientName);
        serviceLigneCommande =  API.getApi(API.LIGNE_COMMANDE).create(ServiceLigneCommande.class);
        getAll();
    }

    private void getAll() {
        try {
            listCall = serviceLigneCommande.listLigneCommande();
            listCall.enqueue(new Callback<ArrayList<Ligne_Commande>>() {
                @Override
                public void onResponse(Call<ArrayList<Ligne_Commande>> call, Response<ArrayList<Ligne_Commande>> response) {
                    adapter = new ListLigneCommandeAdapter(getApplicationContext(),getSupportFragmentManager(),response.body());
                    ligneCommande.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<ArrayList<Ligne_Commande>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),R.string.operation_error,Toast.LENGTH_SHORT).show();
                }
            });
        } catch (NullPointerException npe){
            Toast.makeText(getApplicationContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }
}
