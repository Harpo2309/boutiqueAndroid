package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ricardo on 09/01/2018.
 */

public class Ligne_Commande implements Serializable {

    private String reference;
    private String nomArticle;
    private int quantite;

    public Ligne_Commande(){}

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
