package fr.ul.iutmetz.info.wce.boutiqueandroid.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.PagerAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.article.ArticleAddDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.categorie.CategorieAddDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.client.ClientAddDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.promotion.PromotionAddDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;
    private FloatingActionButton fab_add;
    private DialogFragment addItemDialog;

    // Items to add
    private Categorie categorie;


    // CONSTANTS
    private static final int MENU_BOUTIQUE = 0;
    private static final int MENU_SALES = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        fab_add = findViewById(R.id.fb_add);
        fab_add.setImageResource(R.drawable.ic_add_categorie);
        fab_add.setTag(R.drawable.ic_add_categorie);

        fab_add.setOnClickListener(new View.OnClickListener() {
            /**
             * Changing icon depending fragment
             * @param view
             */
            @Override
            public void onClick(View view) {
                Integer addType = (Integer) fab_add.getTag();
                switch (addType) {
                    case R.drawable.ic_add_categorie:
                        setInsertDialogTo(new CategorieAddDialog());
                        break;
                    case R.drawable.ic_add_article:
                        setInsertDialogTo(new ArticleAddDialog());
                        break;
                    case R.drawable.ic_add_promotion:
                        PromotionAddDialog promotionAddDialog = new PromotionAddDialog();
                        promotionAddDialog.show(getSupportFragmentManager(),"addPromotion");
                        break;
                    case R.drawable.ic_add_client:
                        setInsertDialogTo(new ClientAddDialog());
                        break;
                    case R.drawable.ic_add_commande:
                        Toast.makeText(getApplicationContext(),"Agregar commande",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Tabs in  menu
        tabLayout = findViewById(R.id.tabLayout);
        settingTabsTo(MENU_BOUTIQUE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Viewpager in menu
        viewPager = findViewById(R.id.viewPager);
        settingAdapterTo(MENU_BOUTIQUE);


        // Setting a listener in tabs
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
                settingFloatingButton(adapter.getItemClassName(position));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            settingTabsTo(MENU_BOUTIQUE);
            settingAdapterTo(MENU_BOUTIQUE);
            settingFloatingButton(adapter.getItemClassName(0));
        } else if (id == R.id.nav_sale) {
            settingTabsTo(MENU_SALES);
            settingAdapterTo(MENU_SALES);
            settingFloatingButton(adapter.getItemClassName(0));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void settingTabsTo(int menuType) {
        tabLayout.removeAllTabs();
        switch (menuType) {
            case MENU_BOUTIQUE: // Boutique
                tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_category));
                tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_article));
                tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_promotion));
                break;
            case MENU_SALES: // Sales
                tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_client));
                tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_order));
                break;
        }
    }

    private void settingAdapterTo(int menuType) {
        adapter = new PagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount(), menuType);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void settingFloatingButton(String fragmentClassName) {
        switch (fragmentClassName) {
            case "CategorieFragment":
                fab_add.setImageResource(R.drawable.ic_add_categorie);
                fab_add.setTag(R.drawable.ic_add_categorie);
                break;
            case "ArticleFragment":
                fab_add.setImageResource(R.drawable.ic_add_article);
                fab_add.setTag(R.drawable.ic_add_article);
                break;
            case "PromotionFragment":
                fab_add.setImageResource(R.drawable.ic_add_promotion);
                fab_add.setTag(R.drawable.ic_add_promotion);
                break;
            case "ClientFragment":
                fab_add.setImageResource(R.drawable.ic_add_client);
                fab_add.setTag(R.drawable.ic_add_client);
                break;
            case "CommandeFragment":
                fab_add.setImageResource(R.drawable.ic_add_commande);
                fab_add.setTag(R.drawable.ic_add_commande);
                default:
                    // do sometthig
                    break;
        }
    }

    private void setInsertDialogTo(DialogFragment dialogTarget) {
        addItemDialog = dialogTarget;
        addItemDialog.show(getSupportFragmentManager(),"insert");
    }

}
