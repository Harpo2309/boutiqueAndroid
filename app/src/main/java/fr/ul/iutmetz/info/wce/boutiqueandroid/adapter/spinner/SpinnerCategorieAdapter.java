package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;

/**
 * Created by Celica on 22/01/2018.
 */

public class SpinnerCategorieAdapter extends ArrayAdapter<Categorie> {

    private Context context;
    private FragmentManager fragmentManager;

    public SpinnerCategorieAdapter(Context context, FragmentManager fragmentManager, ArrayList<Categorie> visuelArrayList) {
        super(context,R.layout.spinner_nom_categorie,visuelArrayList);
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.spinner_nom_categorie,viewGroup,false);
        }
        initUI(convertView, getItem(position));
        return convertView;
    }

    @Override
    public View getDropDownView (int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.spinner_nom_categorie,viewGroup,false);
        }
        initUI(convertView, getItem(position));
        return convertView;
    }

    private  void initUI(View convertView, Categorie categorie) {

        ImageView icon = convertView.findViewById(R.id.sp_icon_categorie_nom);
        TextView categorieNom = convertView.findViewById(R.id.sp_categorie_nom);
        categorieNom.setText(categorie.getNom());
        Picasso.with(context)
                .load(API.BASE_ICONS_CATEGORIE + categorie.getVisuel() + API.EXTENSION_ICONS).into(icon);
        if (icon.getDrawable() == null) {
            try {
                InputStream inputStream = context.getAssets().open("not_found.png");
                Drawable substitut = Drawable.createFromStream(inputStream,null);
                icon.setImageDrawable(substitut);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
