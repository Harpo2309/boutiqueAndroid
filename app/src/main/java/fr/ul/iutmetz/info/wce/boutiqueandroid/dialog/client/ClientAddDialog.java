package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.client;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoClientImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;

/**
 * Created by Ricardo on 26/01/2018.
 */

public class ClientAddDialog extends DialogFragment {

    private Client client;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_client_update,null);

        final EditText nomClient = viewUpdateDialog.findViewById(R.id.et_nom_client);
        final EditText prenomClient = viewUpdateDialog.findViewById(R.id.et_prenom_client);
        final EditText villeClient = viewUpdateDialog.findViewById(R.id.et_ville_client);
        final TextView title = viewUpdateDialog.findViewById(R.id.title_dialog_client);

        title.setText(R.string.df_client_update);

        builder.setPositiveButton(R.string.df_client_add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                client = new Client();
                try {
                    client.setNom(nomClient.getText().toString().toUpperCase());

                    client.setPrenom(prenomClient.getText().toString()
                            .replaceFirst(String.valueOf(prenomClient.getText()
                                    .charAt(0)),String.valueOf(prenomClient.getText()
                                    .charAt(0)).toUpperCase()));

                    client.setVille(villeClient.getText().toString()
                            .replaceFirst(String.valueOf(villeClient.getText()
                                    .charAt(0)),String.valueOf(villeClient.getText()
                                    .charAt(0)).toUpperCase()));

                    new DaoClientImpl().add(client,getContext());
                } catch (NullPointerException | IllegalArgumentException | IndexOutOfBoundsException npe) {
                    Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
                }

            }
        }).setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.setView(viewUpdateDialog);

        return builder.create();
    }
}
