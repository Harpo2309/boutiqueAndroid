package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.article;

import android.app.Dialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.DialogFragment;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceArticle;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;

import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerCategorieAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerVisuelArticleAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoArticleImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel_Article;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Celica on 29/01/2018.
 */

public class ArticleAddDialog extends DialogFragment {

    private Article article;
    private int position;
    private Categorie categorie;
    private int positionCat;
    private Spinner spinnerArticle;
    private Spinner spinnerCategorie;
    private ServiceArticle serviceArticle;
    private ServiceCategorie serviceCategorie;
    private Call<ArrayList<Visuel_Article>> listCall;
    private Call<ArrayList<Categorie>> listCallCategorie;
    private SpinnerVisuelArticleAdapter adapter;
    private SpinnerCategorieAdapter adapterAllCategorie;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_article_update,null);

        final EditText reference = viewUpdateDialog.findViewById(R.id.et_reference);
        final EditText nom = viewUpdateDialog.findViewById(R.id.et_nom);
        final EditText tarif = viewUpdateDialog.findViewById(R.id.et_tarif);
        final TextView title = viewUpdateDialog.findViewById(R.id.title_dialog_article);

        title.setText(R.string.df_article_add);
        // Spiner pour l'image du article
        serviceArticle  = API.getApi(API.VISUEL_ARTICLE).create(ServiceArticle.class);

       try {
           listCall = serviceArticle.listVisuelArticle();
           listCall.enqueue(new Callback<ArrayList<Visuel_Article>>() {
               @Override
               public void onResponse(Call<ArrayList<Visuel_Article>> call, Response<ArrayList<Visuel_Article>> response) {
                   if (getContext() != null){
                       adapter = new SpinnerVisuelArticleAdapter(getContext(),getFragmentManager(),response.body());
                       adapter.setDropDownViewResource(R.layout.spinner_item_article);
                       spinnerArticle = getDialog().findViewById(R.id.sp_article);
                       spinnerArticle.setSelection(position);
                       spinnerArticle.setAdapter(adapter);
                   }
               }
               @Override
               public void onFailure(Call<ArrayList<Visuel_Article>> call, Throwable t) {
                   if (getContext() != null){
                       Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                   }
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
       }

        // Spiner pour choisir la categorie
        //setCategorie((Categorie) getArguments().getSerializable("categorie"));
        serviceCategorie  = API.getApi(API.CATEGORIE).create(ServiceCategorie.class);

       try{
           listCallCategorie = serviceCategorie.listCategorie();
           listCallCategorie.enqueue(new Callback<ArrayList<Categorie>>() {
               @Override
               public void onResponse(Call<ArrayList<Categorie>> call, Response<ArrayList<Categorie>> response) {
                  if (getContext() != null){
                      adapterAllCategorie = new SpinnerCategorieAdapter(getContext(),getFragmentManager(),response.body());
                      adapterAllCategorie.setDropDownViewResource(R.layout.spinner_nom_categorie);
                      spinnerCategorie = getDialog().findViewById(R.id.sp_all_categorie);
                      spinnerCategorie.setSelection(positionCat);
                      spinnerCategorie.setAdapter(adapterAllCategorie);
                  }
               }
               @Override
               public void onFailure(Call<ArrayList<Categorie>> call, Throwable t) {
                   if (getContext() != null){
                       Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                   }
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
       }

        builder.setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                article = new Article();
                try {
                    article.setReference(reference.getText().toString());

                    article.setNom(nom.getText().toString()
                            .replaceFirst(String.valueOf(nom.getText()
                                    .charAt(0)),String.valueOf(nom.getText()
                                    .charAt(0)).toUpperCase()));

                    article.setTarif(Double.parseDouble(tarif.getText().toString()));
                    article.setVisuel(spinnerArticle.getSelectedItem().toString());
                    categorie =  (Categorie)spinnerCategorie.getSelectedItem();
                    article.setIdCategorie(categorie.getId_categorie());
                    DaoArticleImpl.getInstance().add(article,getContext());
                } catch (NullPointerException | IllegalArgumentException | IndexOutOfBoundsException npe) {
                    Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
                }

            }
        }).setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        builder.setView(viewUpdateDialog);

        return builder.create();
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public int getPositionCat() {
        return positionCat;
    }

    public void setPositionCat(int positionCat) {
        this.positionCat = positionCat;
    }
}
