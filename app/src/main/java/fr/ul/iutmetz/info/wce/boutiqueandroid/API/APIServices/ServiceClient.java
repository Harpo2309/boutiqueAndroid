package fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Ricardo on 20/01/2018.
 */

public interface ServiceClient {

    @GET("findAll.php")
    Call<ArrayList<Client>> listClients();

    @GET("update.php")
    Call<OperationStatus> updateClient(@Query("nom") String nom, @Query("prenom") String prenom, @Query("ville") String ville, @Query("id_client") int id_client);

    @GET("insert.php")
    Call<OperationStatus> addClient(@Query("nom") String nom, @Query("prenom") String prenom, @Query("ville") String ville, @Query("id_client") int id_client);

    @GET("delete.php")
    Call<OperationStatus> deleteClient(@Query("id_client") int id_client);

}
