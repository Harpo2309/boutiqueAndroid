package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ricardo on 09/01/2018.
 */

public class Commande implements Serializable {

    private int idCommande;
    private String date;
    private Client client;

    private static Commande instance;

    private Commande (int idCommande) {
        this.idCommande = idCommande;
    }

    public static Commande fromIdCommande(int idCommande) {
        if (instance == null)
            instance = new Commande(idCommande);
        else
            instance.setIdCommande(idCommande);
        return instance;
    }

    public Commande(int idCommande, String date, Client client) {
        setIdCommande(idCommande);
        setDate(date);
        setClient(client);
    }

    public Commande(String date, Client client) {
        setDate(date);
        setClient(client);
    }

    public int getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(int idCommande) {
        if(idCommande < 1) {
            throw new IllegalArgumentException("L'identifiant de la commande n'est pas valide");
        } else {
            this.idCommande = idCommande;
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        if (date == null) {
            throw new IllegalArgumentException("La date ne peut pas être null");
        } else {
            this.date = date;
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public static Commande parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        Commande commande = gson.fromJson(response,Commande.class);
        return  commande;
    }
}
