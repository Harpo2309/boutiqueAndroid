package fr.ul.iutmetz.info.wce.boutiqueandroid.dao;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.DAO;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ricardo on 21/01/2018.
 */

public class DaoCategorieImpl implements DAO<Categorie> {

    private Call<OperationStatus> operationStatusCall;
    private ServiceCategorie serviceCategorie;
    private String nomCat;
    private String visuel;

    private static DaoCategorieImpl instance;

    private DaoCategorieImpl() {
        serviceCategorie  = API.getApi(API.CATEGORIE).create(ServiceCategorie.class);
    }

    public static DaoCategorieImpl getInstance() {
        if (instance == null) {
            instance = new DaoCategorieImpl();
        }
        return instance;
    }

    @Override
    public void add(Categorie object, final Context context) {
        nomCat = object.getNom();
        visuel = object.getVisuel();

        try{
            operationStatusCall = serviceCategorie.addCategorie(nomCat,visuel);
            operationStatusCall.enqueue(new Callback<OperationStatus>() {
                @Override
                public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, R.string.operation_add_success,Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OperationStatus> call, Throwable t) {
                    Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void update(Categorie object, final Context context) {
        nomCat = object.getNom();
        visuel = object.getVisuel();
        int idCat = object.getId_categorie();
       try{
           operationStatusCall = serviceCategorie.updateCategorie(nomCat,visuel,idCat);
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_update_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }

    }

    @Override
    public void delete(Categorie object, final Context context) {
        try {
            operationStatusCall = serviceCategorie.deleteCategorie(object.getId_categorie());
            operationStatusCall.enqueue(new Callback<OperationStatus>() {
                @Override
                public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, R.string.operation_delete_success,Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OperationStatus> call, Throwable t) {
                    Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }
}
