package fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices;

import java.util.ArrayList;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel_Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
/**
 * Created by Celica on 23/01/2018.
 */

public interface ServiceArticle {

    @GET("findAll.php")
    Call<ArrayList<Article>> listArticles();

    @GET("findAllNotPromotion.php")
    Call<ArrayList<Article>> listArticlesNotPromotion();

    @GET("update.php")
    Call<OperationStatus> updateArticle(@Query("id_article") int id_article, @Query("reference") String reference ,
                                        @Query("nom") String nom, @Query("tarif") double tarif, @Query("visuel") String visuel,@Query("idCategorie") int idCategorie);

    @GET("delete.php")
    Call<OperationStatus> deleteArticle(@Query("id_article") int id_article);

    @GET("findAllVisuelArticle.php")
    Call<ArrayList<Visuel_Article>> listVisuelArticle();

    @GET("insert.php")
    Call<OperationStatus> addArticle(@Query("reference") String reference, @Query("nom") String nom, @Query("tarif") double tarif, @Query("visuel") String visuel, @Query("idCategorie") int idCategorie);
}
