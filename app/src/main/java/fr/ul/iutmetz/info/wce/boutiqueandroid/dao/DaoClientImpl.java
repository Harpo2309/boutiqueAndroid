package fr.ul.iutmetz.info.wce.boutiqueandroid.dao;


import android.content.Context;
import android.widget.Toast;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceClient;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Client;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.DAO;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ricardo on 24/01/2018.
 */

public class DaoClientImpl implements DAO<Client> {

    private Call<OperationStatus> operationStatusCall;
    private ServiceClient serviceClient;

    private static DaoClientImpl instance;

    public DaoClientImpl() {
        serviceClient = API.getApi(API.CLIENT).create(ServiceClient.class);
    }

    public static DaoClientImpl getInstance() {
        if (instance == null) {
            instance = new DaoClientImpl();
        }
        return instance;
    }

    @Override
    public void add(Client object, final Context context) {
        String nom = object.getNom();
        String prenom = object.getPrenom();
        String ville = object.getVille();
        int idClient = object.getId_client();
       try{
           operationStatusCall = serviceClient.addClient(nom,prenom,ville,idClient);
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_add_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public void update(Client object, final Context context) {
        String nom = object.getNom();
        String prenom = object.getPrenom();
        String ville = object.getVille();
        int idClient = object.getId_client();
       try{
           operationStatusCall = serviceClient.updateClient(nom,prenom,ville,idClient);
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_update_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public void delete(Client object, final Context context) {
       try{
           operationStatusCall = serviceClient.deleteClient(object.getId_client());
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_delete_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }
}
