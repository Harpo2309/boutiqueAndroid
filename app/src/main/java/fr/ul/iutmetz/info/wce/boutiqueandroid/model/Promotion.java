package fr.ul.iutmetz.info.wce.boutiqueandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ricardo on 09/01/2018.
 */

public class Promotion extends Article implements Serializable {

    @SerializedName("dateDebut")
    private String dateDebutPromotion;
    @SerializedName("dateFin")
    private String dateFinPromotion;
    @SerializedName("pourcentage")
    private int pourcentageReduction;

    private static Promotion inststance;

    private Promotion(int idArticle) {
        super(idArticle);
    }

    public Promotion(){}

    public static Promotion fromIdArticle(int idarticle) {
        if (inststance == null)
            inststance = new Promotion(idarticle);
        else
            inststance.setId_article(idarticle);
        return inststance;
    }

    public Promotion(int idArticle, String dateDebutPromotion, String dateFinPromotion, int pourcentageReduction) {
        setId_article(idArticle);
        setDateDebutPromotion(dateDebutPromotion);
        setDateFinPromotion(dateFinPromotion);
        setPourcentageReduction(pourcentageReduction);
    }


    public String getDateDebutPromotion() {
        return dateDebutPromotion;
    }

    public void setDateDebutPromotion(String dateDebutPromotion) {
        if (dateDebutPromotion == null) {
            throw new IllegalArgumentException("La date debut ne peut pas être null");
        } else {
            this.dateDebutPromotion = dateDebutPromotion;
        }
    }

    public String getDateFinPromotion() {
        return dateFinPromotion;
    }

    public void setDateFinPromotion(String dateFinPromotion) {
        if (dateFinPromotion == null) {
            throw new IllegalArgumentException("La date fin ne peut pas être null");
        } else {
            this.dateFinPromotion = dateFinPromotion;
        }
    }

    public int getPourcentageReduction() {
        return pourcentageReduction;
    }

    public void setPourcentageReduction(int pourcentageReduction) {
        if (pourcentageReduction < 1) {
            throw new IllegalArgumentException("Le pourcentage ne peut pas être moins que 1");
        } else {
            this.pourcentageReduction = pourcentageReduction;
        }
    }
}
