package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.promotion;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceArticle;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerArticleAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerVisuelArticleAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoPromotionImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DatePickerGeneral;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel_Article;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ricardo on 08/02/2018.
 */

public class PromotionAddDialog extends DialogFragment implements Serializable, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    // Adapters and data for view elements
    private ServiceArticle serviceArticle;
    private Call<ArrayList<Article>> listCallArticle;
    private SpinnerArticleAdapter adapterArticle;

    // View Elements
    private TextView dialogTitle;
    private Spinner spinnerArticle;
    private TextView promotionPourcentageView;
    private TextView promotionStartDateView;
    private TextView promotionEndDateView;
    private SeekBar promotionPourcentageHandler;
    private ImageView promotionStartDate;
    private ImageView promotionEndDate;

    public PromotionAddDialog(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_promotion_add,null);

        // promotion = (Promotion) getArguments().getSerializable("promotion");
        dialogTitle = viewUpdateDialog.findViewById(R.id.dialog_title);
        promotionPourcentageView = viewUpdateDialog.findViewById(R.id.et_promo_pourcentage);
        promotionStartDateView = viewUpdateDialog.findViewById(R.id.tv_promo_start_date);
        promotionEndDateView = viewUpdateDialog.findViewById(R.id.tv_promo_end_date);
        promotionPourcentageHandler = viewUpdateDialog.findViewById(R.id.sb_promotion_pourcentage);
        promotionStartDate = viewUpdateDialog.findViewById(R.id.iv_promo_start_date);
        promotionEndDate = viewUpdateDialog.findViewById(R.id.iv_promo_end_date);


        serviceArticle  = API.getApi(API.ARTICLE).create(ServiceArticle.class);
        // setting data to view elements
        dialogTitle.setText(R.string.df_promo_add);
        promotionPourcentageHandler.setProgress(25);
        promotionPourcentageView.setText(promotionPourcentageHandler.getProgress() + "%");
        try{
            listCallArticle = serviceArticle.listArticlesNotPromotion();
            listCallArticle.enqueue(new Callback<ArrayList<Article>>() {
                @Override
                public void onResponse(Call<ArrayList<Article>> call, Response<ArrayList<Article>> response) {
                    if (getContext() != null){
                        adapterArticle = new SpinnerArticleAdapter(getContext(),getFragmentManager(),response.body());
                        adapterArticle.setDropDownViewResource(R.layout.spinner_item_article);
                        spinnerArticle = getDialog().findViewById(R.id.sp_article);
                        spinnerArticle.setAdapter(adapterArticle);
                    }
                }
                @Override
                public void onFailure(Call<ArrayList<Article>> call, Throwable t) {
                    if(getContext() != null){
                        Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
        }

        // setting listeners
        promotionStartDate.setOnClickListener(this);
        promotionEndDate.setOnClickListener(this);
        promotionPourcentageHandler.setOnSeekBarChangeListener(this);

        builder.setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Promotion promotion = new Promotion();
                Article article = (Article) spinnerArticle.getSelectedItem();
                if (promotionPourcentageHandler.getProgress() > 1) {
                    try {
                        promotion.setDateDebutPromotion(promotionStartDateView.getText().toString());
                        promotion.setDateFinPromotion(promotionEndDateView.getText().toString());
                        promotion.setId_article(article.getId_article());
                        promotion.setPourcentageReduction(promotionPourcentageHandler.getProgress());
                        DaoPromotionImpl.getInstance().add(promotion,getContext());
                    } catch (Exception ex) {
                        Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        builder.setView(viewUpdateDialog);

        return builder.create();
    }

    /**
     * OnClickListener for trigger datePicker
     * @param view datePicker
     */
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            // start date
            case R.id.iv_promo_start_date:
                displayDatePickerTo(promotionStartDateView);
                break;
            // end date
            case R.id.iv_promo_end_date:
                displayDatePickerTo(promotionEndDateView);
                break;
        }
    }

    // Show datePicker
    private void displayDatePickerTo(final TextView view) {
        DatePickerGeneral datePiker;
        datePiker = DatePickerGeneral.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = year + "-" + (month + 1) + "-" + day;
                view.setText(selectedDate);
            }
        });
        datePiker.show(getActivity().getSupportFragmentManager(),"datePicker");
    }

    // Progressbar controllers

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        promotionPourcentageView.setText(i + "%");
        if (i < 1)
            promotionPourcentageView.setTextColor(getResources().getColor(R.color.colorRed));
        else {
            promotionPourcentageView.setTextColor(getResources().getColor(R.color.colorBlack));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

}
