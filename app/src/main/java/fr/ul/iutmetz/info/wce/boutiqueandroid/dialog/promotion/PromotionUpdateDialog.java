package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.promotion;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DatePickerGeneral;

import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoPromotionImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;

/**
 * Created by Ricardo on 08/02/2018.
 */

public class PromotionUpdateDialog extends DialogFragment implements SeekBar.OnSeekBarChangeListener {

    private Promotion promotion;

    // View Elements
    private TextView dialogTitle;
    private Spinner spinnerArticle;
    private TextView promotionPourcentageView;
    private TextView promotionStartDateView;
    private TextView promotionEndDateView;
    private SeekBar promotionPourcentageHandler;
    private ImageView promotionStartDate;
    private ImageView promotionEndDate;

    public PromotionUpdateDialog(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_promotion_update,null);

        promotion = (Promotion) getArguments().getSerializable("promotion");

        dialogTitle = viewUpdateDialog.findViewById(R.id.dialog_title);
        promotionPourcentageView = viewUpdateDialog.findViewById(R.id.et_promo_pourcentage);
        promotionStartDateView = viewUpdateDialog.findViewById(R.id.tv_promo_start_date);
        promotionEndDateView = viewUpdateDialog.findViewById(R.id.tv_promo_end_date);
        promotionPourcentageHandler = viewUpdateDialog.findViewById(R.id.sb_promotion_pourcentage);
        promotionStartDate = viewUpdateDialog.findViewById(R.id.iv_promo_start_date);
        promotionEndDate = viewUpdateDialog.findViewById(R.id.iv_promo_end_date);

        dialogTitle.setText(R.string.df_promo_update);
        promotionPourcentageHandler.setProgress(promotion.getPourcentageReduction());
        promotionStartDateView.setText(promotion.getDateDebutPromotion());
        promotionEndDateView.setText(promotion.getDateFinPromotion());
        promotionPourcentageView.setText(promotion.getPourcentageReduction() + "%");

        promotionStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDatePickerTo(promotionStartDateView);
            }
        });

        promotionEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDatePickerTo(promotionEndDateView);
            }
        });

        promotionPourcentageHandler.setOnSeekBarChangeListener(this);

        builder.setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (promotionPourcentageHandler.getProgress() > 1) {
                    try {
                        if (!promotionStartDateView.getText().toString().equals(""))
                            promotion.setDateDebutPromotion(promotionStartDateView.getText().toString());
                        if (!promotionEndDateView.getText().toString().equals(""))
                            promotion.setDateFinPromotion(promotionEndDateView.getText().toString());
                        DaoPromotionImpl.getInstance().update(promotion,getContext());
                    } catch (NullPointerException | IllegalArgumentException | IndexOutOfBoundsException npe) {
                        Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        builder.setView(viewUpdateDialog);

        return builder.create();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        promotionPourcentageView.setText(i + "%");
        if (i < 1)
            promotionPourcentageView.setTextColor(getResources().getColor(R.color.colorRed));
        else {
            promotionPourcentageView.setTextColor(getResources().getColor(R.color.colorBlack));
            promotion.setPourcentageReduction(i);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    // Show datePicker
    private void displayDatePickerTo(final TextView view) {
        DatePickerGeneral datePiker;
        datePiker = DatePickerGeneral.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                final String selectedDate = year + "-" + (month + 1) + "-" + day;
                view.setText(selectedDate);
            }
        });
        datePiker.show(getActivity().getSupportFragmentManager(),"datePicker");
    }
}
