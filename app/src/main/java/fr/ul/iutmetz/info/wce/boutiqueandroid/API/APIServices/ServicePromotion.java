package fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices;

import java.util.ArrayList;
import java.util.Date;

import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Promotion;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ricardo on 01/02/2018.
 */

public interface ServicePromotion {

    @GET("findAll.php")
    Call<ArrayList<Promotion>> listPromotion();

    @GET("update.php")
    Call<OperationStatus> uodatePromotion(@Query("dateDebut") String dateDebut, @Query("dateFin") String dateFin, @Query("pourcentage") int pourcentage, @Query("idArticle")int idArticle);

    @GET("insert.php")
    Call<OperationStatus> addPromotion(@Query("idArticle") int idArticle, @Query("dateDebut") String dateDebut, @Query("dateFin") String dateFin,@Query("pourcentage") int pourcentage);

    @GET("delete.php")
    Call<OperationStatus> deletePromotion(@Query("idArticle") int idArticle);
}
