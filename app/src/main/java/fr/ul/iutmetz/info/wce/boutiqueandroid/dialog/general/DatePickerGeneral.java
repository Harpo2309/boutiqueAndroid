package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import java.util.Calendar;

/**
 * Created by Ricardo on 08/02/2018.
 */

public class DatePickerGeneral extends DialogFragment {

    private DatePickerDialog.OnDateSetListener dateListener;

    public static DatePickerGeneral newInstance(DatePickerDialog.OnDateSetListener listener) {
        DatePickerGeneral datePicker = new DatePickerGeneral();
        datePicker.setListener(listener);
        return datePicker;
    }

    public void setListener(DatePickerDialog.OnDateSetListener dateListener) {
        this.dateListener = dateListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return  new DatePickerDialog(getActivity(),dateListener,year,month,day);
    }
}
