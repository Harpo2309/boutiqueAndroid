package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.activity.CommandeActivity;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCommandeImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.categorie.CategorieUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.commande.CommandeUpdateDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.general.DeleteDialog;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Commande;

/**
 * Created by Célica on 27/02/2018.
 */

public class ListCommandeAdapter extends ArrayAdapter<Commande> implements View.OnClickListener, Serializable, DialogInterface.OnClickListener {

    private Context context;
    private FragmentManager fragmentManager;
    private Drawable substitut;
    private ArrayList<Commande> commandeArrayList;
    private int position;

    public ListCommandeAdapter(Context context, FragmentManager fragmentManager, ArrayList<Commande> commandeArrayList) {
        super(context,0,commandeArrayList);
        this.context = context;
        this.commandeArrayList = commandeArrayList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.list_item_commande,viewGroup,false);
        }
        initUI(convertView, getItem(position),position);
        return convertView;
    }

    private  void initUI(View convertView, Commande commande, final int position) {

        //ImageView icon = convertView.findViewById(R.id.cl_icon);
        ImageView ligne = convertView.findViewById(R.id.cl_ligne_commande);
        ImageView delete = convertView.findViewById(R.id.cl_delete_commande);
        TextView date = convertView.findViewById(R.id.cl_date);
        TextView nomClient = convertView.findViewById(R.id.cl_nom);
        TextView prenomClient = convertView.findViewById(R.id.cl_prenom);

        delete.setTag(String.valueOf(position));
        ligne.setTag(String.valueOf(position));
        date.setText(commande.getDate());
        nomClient.setText(commande.getClient().getNom());
        prenomClient.setText(commande.getClient().getPrenom());

        // Setting images

        try {
            InputStream inputStream = context.getAssets().open("car.png");
            this.substitut = Drawable.createFromStream(inputStream,null);
            ligne.setImageDrawable(substitut);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try {
            InputStream inputStream = context.getAssets().open("delete.png");
            this.substitut = Drawable.createFromStream(inputStream,null);
            delete.setImageDrawable(substitut);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        delete.setOnClickListener(this);
        ligne.setOnClickListener(this);
    }

    /**
     * OnClickListener for Edit and Delete buttons
     * @param view Button
     */
    @Override
    public void onClick(View view) {
        Bundle args;
        position = Integer.parseInt(view.getTag().toString());
        switch (view.getId()) {
            // Updating
            case R.id.cl_ligne_commande:
                Intent intent = new Intent(getContext(), CommandeActivity.class);
                intent.putExtra("clientName",getItem(position).getClient().getNom());
                intent.putExtra("clientLastName",getItem(position).getClient().getPrenom());
                getContext().startActivity(intent);
                break;
            // Deleting
            case R.id.cl_delete_commande:
                DeleteDialog dialogDelete = new DeleteDialog();
                args = new Bundle();
                args.putString("typeItem","commande");
                args.putSerializable("listenerFromAdapter",this);
                dialogDelete.setArguments(args);
                dialogDelete.show(fragmentManager,"delete");
                break;
        }
    }

    /**
     * OnClickListener for DialogFragment Delete
     * @param dialogInterface ?
     * @param i ?
     */
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Commande commande = Commande.fromIdCommande(getItem(position).getIdCommande());
        DaoCommandeImpl.getInstance().delete(commande,getContext());
        commandeArrayList.remove(getItem(position));
        notifyDataSetChanged();
    }
}
