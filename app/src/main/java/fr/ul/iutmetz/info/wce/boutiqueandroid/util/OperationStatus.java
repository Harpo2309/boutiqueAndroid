package fr.ul.iutmetz.info.wce.boutiqueandroid.util;

/**
 * Created by Ricardo on 21/01/2018.
 */

public class OperationStatus {

    private String status;
    private String type;

    public OperationStatus(){}

    public OperationStatus(String status, String type) {
        this.status = status;
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
