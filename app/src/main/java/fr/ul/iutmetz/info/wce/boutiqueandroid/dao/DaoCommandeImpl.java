package fr.ul.iutmetz.info.wce.boutiqueandroid.dao;

import android.content.Context;
import android.widget.Toast;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCommande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Commande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.DAO;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Célica on 01/03/2018.
 */

public class DaoCommandeImpl implements DAO<Commande> {

    private Call<OperationStatus> operationStatusCall;
    private ServiceCommande serviceCommande;
    private String date;
    private int idClient;
    private int idCommande;

    private static DaoCommandeImpl instance;

    private DaoCommandeImpl() {
        serviceCommande  = API.getApi(API.COMMANDE).create(ServiceCommande.class);
    }

    public static DaoCommandeImpl getInstance() {
        if (instance == null) {
            instance = new DaoCommandeImpl();
        }
        return instance;
    }

    @Override
    public void add(Commande object, final Context context) {
        date = object.getDate().toString();
        idClient = object.getClient().getId_client();

        try{
            operationStatusCall = serviceCommande.addCommande(date,idClient);
            operationStatusCall.enqueue(new Callback<OperationStatus>() {
                @Override
                public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, R.string.operation_add_success,Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OperationStatus> call, Throwable t) {
                    Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void update(Commande object, final Context context) {
        date = object.getDate().toString();
        idClient = object.getClient().getId_client();
        idCommande = object.getIdCommande();
       try{
           operationStatusCall = serviceCommande.updateCommande(idCommande,date,idClient);
           operationStatusCall.enqueue(new Callback<OperationStatus>() {
               @Override
               public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                   if (response.isSuccessful()) {
                       Toast.makeText(context, R.string.operation_update_success,Toast.LENGTH_SHORT).show();
                   }
               }

               @Override
               public void onFailure(Call<OperationStatus> call, Throwable t) {
                   Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
               }
           });
       }catch (NullPointerException npe){
           Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
       }

    }

    @Override
    public void delete(Commande object, final Context context) {
        try {
            operationStatusCall = serviceCommande.deleteCommande(object.getIdCommande());
            operationStatusCall.enqueue(new Callback<OperationStatus>() {
                @Override
                public void onResponse(Call<OperationStatus> call, Response<OperationStatus> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, R.string.operation_delete_success,Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OperationStatus> call, Throwable t) {
                    Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(context,R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }
}
