package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.categorie;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerVisuelCategorieAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Celica on 09/02/2018.
 */

public class CategorieAddDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private Categorie categorie;
    private int position;
    private Spinner spinnerCategorie;
    private ServiceCategorie serviceCategorie;
    private Call<ArrayList<Visuel>> listCall;
    private SpinnerVisuelCategorieAdapter adapter;
    private EditText editText;

    public CategorieAddDialog(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewAddDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_categorie_update,null);

        final TextView title = viewAddDialog.findViewById(R.id.title_dialog_category);
        title.setText(R.string.df_category_add);
        serviceCategorie  = API.getApi(API.VISUEL).create(ServiceCategorie.class);

        try{
            listCall = serviceCategorie.listVisuel();
            listCall.enqueue(new Callback<ArrayList<Visuel>>() {
                @Override
                public void onResponse(Call<ArrayList<Visuel>> call, Response<ArrayList<Visuel>> response) {
                    if (getContext() != null){
                        adapter = new SpinnerVisuelCategorieAdapter(getContext(),getFragmentManager(),response.body());
                        adapter.setDropDownViewResource(R.layout.spinner_item_categorie);
                        spinnerCategorie = getDialog().findViewById(R.id.sp_categorie);
                        spinnerCategorie.setSelection(position);
                        spinnerCategorie.setAdapter(adapter);
                    }
                }
                @Override
                public void onFailure(Call<ArrayList<Visuel>> call, Throwable t) {
                    if (getContext() != null){
                        Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
        }

        builder.setPositiveButton(R.string.df_category_add, this);
        builder.setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            dismiss();
            }
        });

        builder.setView(viewAddDialog);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        editText = getDialog().findViewById(R.id.et_nom_categorie);
        categorie = new Categorie();
        try {
            categorie.setNom(editText.getText().toString());
            categorie.setVisuel(spinnerCategorie.getSelectedItem().toString());
            DaoCategorieImpl.getInstance().add(categorie,getContext());
        } catch (NullPointerException | IllegalArgumentException ex) {
            Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
        }
    }

    // Getters & setters

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
