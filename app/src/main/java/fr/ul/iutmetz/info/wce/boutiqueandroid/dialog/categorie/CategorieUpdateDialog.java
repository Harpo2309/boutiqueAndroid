package fr.ul.iutmetz.info.wce.boutiqueandroid.dialog.categorie;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner.SpinnerVisuelCategorieAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.dao.DaoCategorieImpl;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ricardo on 21/01/2018.
 */

public class CategorieUpdateDialog extends DialogFragment {

    private Categorie categorie;
    private Spinner spinnerCategorie;
    private ServiceCategorie serviceCategorie;
    private Call<ArrayList<Visuel>> listCall;
    private ArrayList<Visuel> visuelResponse;
    private SpinnerVisuelCategorieAdapter adapter;
    private EditText etNomCategorie;

    public CategorieUpdateDialog(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View viewUpdateDialog = LayoutInflater.from(getContext()).inflate(R.layout.dialog_categorie_update,null);

        categorie = (Categorie) getArguments().getSerializable("categorie");
        serviceCategorie  = API.getApi(API.VISUEL).create(ServiceCategorie.class);
        etNomCategorie = viewUpdateDialog.findViewById(R.id.et_nom_categorie);

        etNomCategorie.setText(categorie.getNom());

        try{
            listCall = serviceCategorie.listVisuel();
            listCall.enqueue(new Callback<ArrayList<Visuel>>() {
                @Override
                public void onResponse(Call<ArrayList<Visuel>> call, Response<ArrayList<Visuel>> response) {
                    if (getContext() != null){
                        adapter = new SpinnerVisuelCategorieAdapter(getContext(),getFragmentManager(),response.body());
                        visuelResponse = response.body();
                        adapter.setDropDownViewResource(R.layout.spinner_item_categorie);
                        spinnerCategorie = viewUpdateDialog.findViewById(R.id.sp_categorie);
                        spinnerCategorie.setAdapter(adapter);
                        Visuel currentVisuel = Visuel.fromNomVisuel(categorie.getVisuel());
                        int currentValue = visuelResponse.indexOf(currentVisuel);
                        spinnerCategorie.setSelection(currentValue);
                    }
                }
                @Override
                public void onFailure(Call<ArrayList<Visuel>> call, Throwable t) {
                   if (getContext() != null){
                       Toast.makeText(getContext(),R.string.error_internet ,Toast.LENGTH_SHORT).show();
                   }
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
        }

        builder.setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                try {
                    categorie.setNom(etNomCategorie.getText().toString());
                    categorie.setVisuel(spinnerCategorie.getSelectedItem().toString());
                    DaoCategorieImpl.getInstance().update(categorie,getContext());
                } catch (NullPointerException | IllegalArgumentException npe) {
                    Toast.makeText(getContext(),R.string.empty_value_name,Toast.LENGTH_SHORT).show();
                }
            }
        })
        .setNegativeButton(R.string.action_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        builder.setView(viewUpdateDialog);
        return builder.create();
    }
}
