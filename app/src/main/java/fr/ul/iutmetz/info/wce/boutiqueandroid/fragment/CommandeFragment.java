package fr.ul.iutmetz.info.wce.boutiqueandroid.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import java.util.ArrayList;

import android.widget.Toast;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCommande;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListCommandeAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Commande;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommandeFragment extends Fragment {
    private ListView listViewCommande;
    private ServiceCommande serviceCommande;
    private Call<ArrayList<Commande>> listCall;
    private ListCommandeAdapter adapter;

    public CommandeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_commande, container, false);
        serviceCommande  = API.getApi(API.COMMANDE).create(ServiceCommande.class);
        initUI(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAll();
    }

    private void initUI(View view) {
        listViewCommande = view.findViewById(R.id.listCommande);
    }

    public void getAll() {
        try{
            listCall = serviceCommande.listCommande();
            listCall.enqueue(new Callback<ArrayList<Commande>>() {
                @Override
                public void onResponse(Call<ArrayList<Commande>> call, Response<ArrayList<Commande>> response) {
                    if (getContext() != null){
                        adapter = new ListCommandeAdapter(getContext(),getFragmentManager(),response.body());
                        listViewCommande.setAdapter(adapter);
                    }
                }
                @Override
                public void onFailure(Call<ArrayList<Commande>> call, Throwable t) {
                    if (getContext() != null){
                        Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (NullPointerException npe){
            Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public String toString() {
        return "CommandeFragment";
    }

}
