package fr.ul.iutmetz.info.wce.boutiqueandroid.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices.ServiceCategorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.list.ListCategorieAdapter;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategorieFragment extends Fragment{

    private ListView listViewCategorie;
    private ServiceCategorie serviceCategorie;
    private Call<ArrayList<Categorie>> listCall;
    private ListCategorieAdapter adapter;

    public CategorieFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_categorie, container, false);
        serviceCategorie  = API.getApi(API.CATEGORIE).create(ServiceCategorie.class);
        initUI(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAll();
    }

    private void initUI(View view) {
        listViewCategorie = view.findViewById(R.id.listCategorie);
    }

    public void getAll() {
       try{
           listCall = serviceCategorie.listCategorie();
           listCall.enqueue(new Callback<ArrayList<Categorie>>() {
               @Override
               public void onResponse(Call<ArrayList<Categorie>> call, Response<ArrayList<Categorie>> response) {
                   if (getContext() != null){
                       adapter = new ListCategorieAdapter(getContext(),getFragmentManager(),response.body());
                       listViewCategorie.setAdapter(adapter);
                   }
               }
               @Override
               public void onFailure(Call<ArrayList<Categorie>> call, Throwable t) {
                   if (getContext() != null){
                       Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_SHORT).show();
                   }
               }
           });
       } catch (NullPointerException npe){
           Toast.makeText(getContext(),R.string.operation_error,Toast.LENGTH_LONG).show();
       }
    }

    @Override
    public String toString() {
        return "CategorieFragment";
    }

}
