package fr.ul.iutmetz.info.wce.boutiqueandroid.API.APIServices;

import java.util.ArrayList;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Categorie;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Visuel;
import fr.ul.iutmetz.info.wce.boutiqueandroid.util.OperationStatus;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ricardo on 20/01/2018.
 */

public interface ServiceCategorie {

    @GET("findAll.php")
    Call<ArrayList<Categorie>> listCategorie();

    @GET("update.php")
    Call<OperationStatus> updateCategorie(@Query("nom") String nom, @Query("visuel") String visuel, @Query("id_categorie") int id_categorie);

    @GET("delete.php")
    Call<OperationStatus> deleteCategorie(@Query("id_categorie") int idCategorie);

    @GET("findAllVisuel.php")
    Call<ArrayList<Visuel>> listVisuel();

    @GET("insert.php")
    Call<OperationStatus> addCategorie(@Query("nom") String nom, @Query("visuel") String visuel);
}
