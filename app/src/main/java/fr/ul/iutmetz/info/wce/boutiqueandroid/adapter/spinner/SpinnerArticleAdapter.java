package fr.ul.iutmetz.info.wce.boutiqueandroid.adapter.spinner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import fr.ul.iutmetz.info.wce.boutiqueandroid.API.API;
import fr.ul.iutmetz.info.wce.boutiqueandroid.R;
import fr.ul.iutmetz.info.wce.boutiqueandroid.model.Article;

/**
 * Created by Ricardo on 28/02/2018.
 */

public class SpinnerArticleAdapter extends ArrayAdapter<Article> {

    private Context context;
    private FragmentManager fragmentManager;

    public SpinnerArticleAdapter(Context context, FragmentManager fragmentManager, ArrayList<Article> articleArrayList) {
        super(context, R.layout.spinner_item_article,articleArrayList);
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.spinner_item_article,viewGroup,false);
        }
        initUI(convertView, getItem(position));
        return convertView;
    }

    @Override
    public View getDropDownView (int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.spinner_item_article,viewGroup,false);
        }
        initUI(convertView, getItem(position));
        return convertView;
    }

    private  void initUI(View convertView, Article article) {

        ImageView icon = convertView.findViewById(R.id.sp_icon_article);
        TextView imageArticleNom = convertView.findViewById(R.id.sp_nom_article);

        imageArticleNom.setText(article.getNom());
        Picasso.with(context)
                .load(API.BASE_ICONS_ARTICLE + article.getVisuel() + API.EXTENSION_ICONS).into(icon);
        if (icon.getDrawable() == null) {
            try {
                InputStream inputStream = context.getAssets().open("not_found.png");
                Drawable substitut = Drawable.createFromStream(inputStream,null);
                icon.setImageDrawable(substitut);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

}
